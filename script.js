
const checkIfValue = (val) => {
    const vals = ["0","1","2","3","4","5","6","7","8","9"];
    let gotIt = false;
    vals.forEach((element)=>{
        if(element==val) {
            gotIt = true;
        }
    })
    return gotIt;
}

let toStore = 0;
let op = null;
document.querySelector("#buttonContainer").addEventListener('click',(e)=>{
    let currentValue = document.querySelector('#output span');
    if(checkIfValue(e.target.innerHTML)){
        currentValue.innerHTML = parseInt(currentValue.innerHTML + e.target.innerHTML);
    }
    else{
        const operator = e.target.innerHTML;
        if(operator == '='){
            console.log(currentValue.innerHTML,toStore,op);
            if(op=='+') {
                currentValue.innerHTML = parseInt(currentValue.innerHTML) + parseInt(toStore);
                toStore = 0; 
            } 
            if(op=='-') {
                currentValue.innerHTML = parseInt(currentValue.innerHTML) - parseInt(toStore);
                toStore = 0; 
            }
            if(op=='*') {
                currentValue.innerHTML = parseInt(currentValue.innerHTML) * parseInt(toStore);
                toStore = 0; 
            }
            if(op=='/' && currentValue.innerHTML!='0') {
                currentValue.innerHTML =  parseInt(toStore) / parseInt(currentValue.innerHTML);
                toStore = 0; 
            }
            op = null;
        } else {
            toStore = currentValue.innerHTML;
            op = operator;
            currentValue.innerHTML = 0;
        }
    }
});